package com.bcp.tipocambio.modelo;

import lombok.Data;

@Data
public class TipoCambioDTO {
	private Float monto;
	private Float montoTipoCambio;
	private String monedaOrigen;
	private String monedaDestino;
	private Float tipoCambio;

	public Float getMonto() {
		return monto;
	}

	public void setMonto(Float monto) {
		this.monto = monto;
	}

	public Float getMontoTipoCambio() {
		return montoTipoCambio;
	}

	public void setMontoTipoCambio(Float montoTipoCambio) {
		this.montoTipoCambio = montoTipoCambio;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public Float getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Float tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

}
