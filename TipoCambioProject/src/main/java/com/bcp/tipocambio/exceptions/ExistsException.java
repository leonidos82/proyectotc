package com.bcp.tipocambio.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ExistsException extends RuntimeException{

	private static final long serialVersionUID = 1L;

public ExistsException(String mensaje){
            super(mensaje);
        }

}
